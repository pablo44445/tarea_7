import { defineStore } from "pinia";

export const useTareas= defineStore("main",{
    state: () => ({
        tareas:[{descripcion:"sacar la basura",completada:false,borrada:false},
        {descripcion:"limpiar la terraza",completada:true ,borrada:false}],
        aceptable:null,
        ver_tareas_completadas :false,
        se_ve_completas:""
    }),
    actions:{
        eliminar_t(index){
            this.tareas[index].borrada=true
        },
        Agregar_t(descripcion){
            this.tareas.push({descripcion:descripcion,completada:false,borrada:false})
        },
        t_completada(index){
            var tarea_aux = this.tareas[index];
            tarea_aux.completada=true;
            this.tareas[index]=tarea_aux;
        },
        t_incompleta(index){
            var tarea_aux = this.tareas[index];
            tarea_aux.completada=false;
            this.tareas[index]=tarea_aux;
        }
    }
    
});